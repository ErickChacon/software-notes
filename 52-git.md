## Some references

- Last 4 commits are denoted as `HEAD`, `HEAD^`, `HEAD~2` and `HEAD~3`.

## Remove changes

- In case we `git add` files, we can reset it with `git reset`.

```bash
git add file.R
git reset
```

- Revert all uncommitted changes:

```bash
git reset --hard HEAD
```

- Remove completely las two commits:

```bash
git reset --hard HEAD~2
```

See more details at [git-reset](https://git-scm.com/docs/git-reset).

## Manage forked repository for contribution

The usual workflow to contribute to external repositories is to:

- make a fork
- add a feature, fix a bug or something else on your fork
- make a pull request.

If your pull request is open, you can add changes to it by simply pushing more
commits to your current fork. The pull request will be automatically updates. 

After the pull request is accepted. It is desirable to update your fork if you
are planning to continue contributing. You can update your fork as follow:

```bash
# https://stackoverflow.com/questions/7244321/how-do-i-update-a-github-forked-repository
git remote add upstream https://github.com/whoever/whatever.git
git fetch upstream
git checkout master
git rebase upstream/master
```

The last command make our unmerged commits to be located at the top of the
upstream branch.

In case there is any merging conflict, just fix this and then
```bash
git add -A
git rebase --continue
```

## Git submodules

Git submodules simply allows us to include a git repository (submodule) inside
another repository (main). This is very useful when a dependency of our main
project is under development.

For example, for the development of a web for my data science project, I use
`hugo` and my theme (`scholar-docs`). This theme defines the structure and
aesthetics of my web. I add the submodule `scholar-docs` inside a folder
`docs/themes` as follows:

```bash
git submodule add git@github.com:ErickChacon/scholar-docs.git docs/themes/scholar-docs
```

The new submodule can be pushed as usually `git commit -am 'add scholar-docs
submodule'`.

### Remove git submodule

Remove completely a submodule located at `docs/themes/scholar`
```bash
rm -rf docs/themes/scholar
git submodule deinit -f -- docs/themes/scholar
rm -rf .git/modules/docs/themes/scholar
git rm -f docs/themes/scholar
```

Remove and keep it
```bash
mv path/to/scholar somewhere/scholar
git submodule deinit -f -- path/to/scholar    
rm -rf .git/modules/path/to/scholar
git rm -f path/to/scholar
git rm --cached path/to/scholar
mv somewhere/scholar path/to/scholar/submodule
```

### go back to specific commit locally and remotely

```bash
git reset --hard <old-commit-id>
git push -f <remote-name> <branch-name>
```

```bash
git init
git pull --allow-unrelated-histories
```

### add remote to exixsting project

```bash
git init
git add -A
git remote add origin ...
git pull origin master
```

### fork

```bash
git clone git@github.com:torrinfail/dwmblocks.git
git remote set-url origin git@github.com:ErickChacon/dwmblocks.git
```
