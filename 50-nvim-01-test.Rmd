# Neovim Text Editor

## Vim wraping lines

do not break lines
```bash
set textwidth=0
# set formatoptions-=tc
```

for specific file (help modeline)

```bash
# vim: tw=0
```

## Profiling


```vim
" begin profiling
:profile start vim-profiling.log

" profile files matching the pattern *
:profile file *

" profile all functions
:profile func *

" execute some commands

" quit
:q
```

## File Explorer (Netrw)

Netrw is the native file explorer of Neovim.

Common ways to open a `netrw` file explorer:

- `Explore` : explore directory of current file,
- `Vexplore`: explore directory of current file on a vertical split,
- `Sexplore`: explore directory of current file on a horizontal split,
- `Lexplore`: explore directory of current file on left split (similar to NERDTree).

For an opened `netrw` split, common commands that control the *appearance* are:

- `I` : toggle the banner (same as `let g:netrw_banner = 0/1`),
- `i` : cycle between listing styles (same as `let g:netrw_liststyle = 0/1/2/3`),
- `gh`: hide/unhide dotfiles.

Common `file operations`:

- `d`: create a new directory,
- `%`: create a new file and open it,
- `D`: delete file/directory (it only delete empty directories),
- `R`: rename file/directory.
- `<ctrl-h>`: update netrw

Another alternative for files operations is to use shell commands using `%` as the current
directory or file path. See examples below.

- `:!mkdir %/dir`: create a new directory called `dir`
- `:!rm -r %/dir`: remove recursively the `dir` directory
- `:!touch %/file.md`: create a new file called `file.md`
- `:e %/newfile.md`: edit `newfile.md`.

Navigation and paths (browsing and current directories):

- `cd`: make current directory the same as the browsing directory (related to `let
  g:netrw_keepdir = 0/1`),
- `gn`: make browsing directory the same as the directory below the cursor,
- `-` : go up one directory,
- `u` : go back to previous directory,
- `U` : go back to subsequent directory.

Browsing:

- `cr`: open files/directories in current window
- `o` : open file in a new horizontal split
- `v` : open file in a new vertical split
- `t` : preview file in a new tab
- `p` : preview file without leaving `netrw`
- `P` : preview file in previous window (does not work as expected)
- `x` : preview file with the default application

## Plugin fzf.vim

[fzf](https://github.com/junegunn/fzf) is a fast command-line fuzzy finder usually used to
find files, and [fzf.vim](https://github.com/junegunn/fzf.vim) is a integration of `fzf`
with `Vim` including additional features to do fuzzy search on files, buffers,
colorschemes, so on.

Some useful commands are:

- `Files`: List files
- `Buffers`: List buffers
- `Lines`: List lines in loaded buffers
- `Blines`: List lines in current buffer
- `Windows`: List windows to move
- `Ag`: List of lines in all (not hidden) files using `ag` (The Silver Searcher).

