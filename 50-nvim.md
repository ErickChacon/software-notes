
## Vimtex
VimtexTocToggle " for table of contents
VimtexCompileToggle " to compile in continuous mode.
VimtexCompileSS " simple compilation
VimtexClean " Clean auxiliary files
VimtexErrors " To open and close errors window
VimtexView " to visualize pdf
check for deoplete later


## Get the current value of a nvim attribute

```
:verbose set conceallevel?
```

Indicates you the current value of that attribute and which file has modified it.

# Introduction to Neovim

## Syntax highlighting

`syntax enable` allows syntax highlighting, this command runs `source $VIMRUNTIME/syntax/syntax.vim`. This is the default option.

It is supposed that `syntax on` overrule your settings with the default in `init.vim`, but I do not find differences with `syntax enable`.

- When using a color scheme, syntax enable or on restore the color scheme.
Example:
```
:colorscheme gruvbox
:highlight Normal guibg=Black guifg=White
:syntax enable
```

- If a color scheme is not used, syntax enable or on do not remove your custom highlights.
Example:
```
:highlight Normal guibg=Black guifg=White
:syntax enable
```

You can find an option on g:syntax_on to toggle between syntax on/off.

### Syntax files (syn-files)

#### Make your own syntax highlighting (mysyntaxfile)

If you want to use your own syntax, locate this on `~/.config/nvim/syntax`.  For example, the file for an `r` syntax should be located on  `~/.config/nvim/syntax/r.vim`

If the syntax is new, create the detecting file on `~/.config/nvim/ftdetect`. For example, `/.config/nvim/ftdetect/stan.vim` for stan. This file should have the following structure.

```
if exists("b:current_syntax")
  finish
endif

" The code goes in this area.

let b:current_syntax = "stan"
```

#### Add an existing syntax file (mysyntaxfile-add)

To add some features to the default syntax, locate your file on `~/.config/nvim/after/syntax`. The file can be, for example, `~/.config/nvim/after/syntax/stan.vim` for stan.

#### Naming conventions (group-name)

Syntax groups are set and, then, linked to highlight groups.

        *Comment	any comment

	*Constant	any constant
	 String		a string constant: "this is a string"
	 Character	a character constant: 'c', '\n'
	 Number		a number constant: 234, 0xff
	 Boolean	a boolean constant: TRUE, false
	 Float		a floating point constant: 2.3e10

	*Identifier	any variable name
	 Function	function name (also: methods for classes)

	*Statement	any statement
	 Conditional	if, then, else, endif, switch, etc.
	 Repeat		for, do, while, etc.
	 Label		case, default, etc.
	 Operator	"sizeof", "+", "*", etc.
	 Keyword	any other keyword
	 Exception	try, catch, throw

	*PreProc	generic Preprocessor
	 Include	preprocessor #include
	 Define		preprocessor #define
	 Macro		same as Define
	 PreCondit	preprocessor #if, #else, #endif, etc.

	*Type		int, long, char, etc.
	 StorageClass	static, register, volatile, etc.
	 Structure	struct, union, enum, etc.
	 Typedef	A typedef

	*Special	any special symbol
	 SpecialChar	special character in a constant
	 Tag		you can use CTRL-] on this
	 Delimiter	character that needs attention
	 SpecialComment	special things inside a comment
	 Debug		debugging statements

	*Underlined	text that stands out, HTML links

	*Ignore		left blank, hidden  |hl-Ignore|

	*Error		any erroneous construct

	*Todo		anything that needs extra attention; mostly the
			keywords TODO FIXME and XXX


### Improving rmd speed on nvim

- it get slow with vim-pandoc-syntax
- by default folding is marker

- do not put pandoc on ftplugin because it removes rmd folding
  - nvim/ftplugin: Generally, if you want to add your own plugin, or replace a
    standard plugin.
  - nvim/after/ftplugin: If you want to keep the functionality of an existing plugin
    but add to it, or change just a few settings.
    
" /usr/share/nvim/runtime/

Error detected while processing command line:
E117: Unknown function: StartRRemoving intermediate container 2d42c38d6835

apt-cache policy to see version of a package

0.3.2, 0.3.3 and 0.3.4 bug with foldcolumn
alternatives --install <link> <name> <path> <priority>

## Markdown improvemens

- [ ] allow lists like vimwiki
- [ ] break text at 90 lines as it writes

## Text wraping (formatoptions)

- t: wrap using textwidth (85)
- a: automatic
- w: trailing white space indicates new paragraph in next line
- n: respect indentation of numbered lists (formatlist)

setlocal formatoptions=twna
setlocal formatlistpat=^\\s*[0-9*]\\+[\\]:.)}\\t\ ]\\s*
let &formatlistpat='^\s*\(\d\+[\]:.)}\t ]\|[*-][\t ]\)\s*'


- writing seminar
- interdisciplinary writing and  publishing
- tips and things in Latex (overleaf)
 
## Vim (custom-syntax)

Documentation at `syn-define`

Three types:

- keyword: only words
`:syntax keyword Type int long char`
- match: regexp pattern `syn-pattern`
`:syntax match Character /'.'/hs=s+1,he=e-1`
- region: with an start regexp pattern and end
`:syntax region String star =+"+ skip=+\\"+ end=+"+`
`:syn region First  start="("  end=":"`
`:syn region Second start="("  end=";"`
`:syntax region Comment  start="/\*"  end="\*/"`

Options:

- matchgroup: define the group used to highlight the start and/or end pattern
  differently than the region's body.
- contains: list of syntax group names to be highlighted inside our matching
  area.
- contained: region will only be recognized when mentioned on the contain
  argument.
- keepend: makes matching of and end pattern of the outer region also end any
  contained item.
- containedin: item will match inside these groups.

After (nvim/after/syntax/filetype.cim):

- Remember to unset b:current_syntax when using `runtime` or `include`.

Syn include:

## Pattern (Concepts)

- Pattern: Matches one or more branches separated by `\|` (e.g. `hola\|bye`)
- Branch: One or more concats separated by `\&` (e.g. `foobepp\&...`, matches foo)
- Concat: One or more pieces concatenated (e.g. `f[0-9]b`)
- Piece: atom with a repetition indication (e.g. `a*`)
- Atom: One of a list of items. (e.g. `[a-c]`)

## Pattern (Magic)

Some characters have an special meaning when placed:
- literally
- with a backslash

- end-of-line `$`
- any character `.`
- any number of previous atom `*`
- 1 or more of previous atom `\+`
- 0 or 1 of previous atom `\?`
- latest substitute string `~`
- grouping into atom `\(\)`
- separating alternatives `\|`
- alphabetic character `\a`
- literal backslash `\\`
- literal dot `\.`
- literal '{' `{`
- literal 'a' `a`

