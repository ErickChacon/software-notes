
sudo apt-get install texlive-fonts-extra

## How to install your own template?

Obtain the directory where you have to put your template.
kpsewhich -var-value=TEXMFHOME
be shure of the directory with, it shows where texlive looks for
kpsepath -n latex tex
check
kpsewhich ResearchNotes.cls

## Latex math convention.

bold italic for matrices
slanted sans serif for tensors

random vector  capital bold
random variable capital
realization no capital

## install packages

find package to be installed

```bash
tlmgr search --global --file "/times.sty"
kpsewhich etoolbox.sty
tlmgr search --global --file "/chicago.bst"
```

