
# Change icon of a program

Details of a program can be found in `/usr/share/applications`; for example it is
found in the file `/usr/share/applications/ranger.desktop` for ranger. If you can to
modify it create the folder `~/.local/share/applications` and include your file
there with the desired modifications.
