
# (PART) Operating System {-}

# Linux utilities

## Directories listing and size

### du: file space usage

`du` is an utility to estimate file space usage. It estimates recursively the disk usage
of the files inside the directory supplied. You can supply diferent sub-directories using
syntax `path/*`. Useful arguments are:

- `-s, --summarize`: display the total space usage for each argument,
- `-h, --human-readable`: print usage in human readable format,
- `-d, --max-depth=N`: print total for subdirectories with depth `N`,
- `--aparent-size`: aparent size is usually smaller,
- `-c, --total`: show grand total.

See some examples below:

```bash
# estimate space usage of current directory
du -sh

# estimate space usage of more than one sub-directory
du -sh dir1 dir

# estimate space usage of all subdirectories
du -sh ./*
du -h -d 1

# estimate space usage of all subdirectories and sort them
du -h -d 1 | sort -rh
```

## Download data

Common command to download data are `wget` and `curl`.

### wget: network downloader

`wget` is an utility to download files from the web. By default, it write the output to a
file with the same name as remote. In case the file exists, it will append an index (`.1`,
`.2`, `.3`, and so on) and to create a new file.

- `-O <file>, --output-document=<file>`: write output to `<file>`,
- `--quiet`: do not show messages.

See some examples below:

```bash
# download a file from github
wget https://raw.githubusercontent.com/ErickChacon/dotfiles-ubuntu-18/master/.config/nvim/init.vim

# download a file from github to standard output
wget -O - https://raw.githubusercontent.com/ErickChacon/dotfiles-ubuntu-18/master/.config/nvim/init.vim

# download rclone installer to a file called `rclone-install.sh`
wget -O rclone-install.sh https://rclone.org/install.sh
```

### curl: transfer data

`curl` is an utility to transfer data from or to a server. I mainly use this to download
files. Common arguments I use in priority order are:

- `-O, --remote-name`: write output to a file with same name as remote,
- `-L, --location`: redirect request in case URL has been modified.
- `-o, --output <file>`: write output to `<file>`,
- `-s, --silent`: do not show progress neither error messages,
- `-S, --show-error`: show error messages,

See some examples below:

```bash
# download a file from github
curl -OL https://raw.githubusercontent.com/ErickChacon/dotfiles-ubuntu-18/master/.config/nvim/init.vim

# download a file from github to standard output
curl https://raw.githubusercontent.com/ErickChacon/dotfiles-ubuntu-18/master/.config/nvim/init.vim

# download rclone installer to a file called `rclone-install.sh`
curl -L -o rclone-install.sh https://rclone.org/install.sh
```

## compress uncompress

```bash
tar xJ
# -J Use xz for compressing or decompressing the archives
```


## Create a bootable installer

- Download the image
- Insert a usb stick
- Copy the installer in the usb stick
```
cp debian-9.0.0-amd64-DVD-1.iso /dev/sda
sync
```

## Adding executable permission to a file

```bash
chmod +x filname
```

## List installed packages and search pattern

```bash
dpkg-query -l
dpkg-query -l 'oni'
```

## Remove and install packages a package

```bash
sudo apt-get remove bla
sudo dpkg -r oni
sudo apt-get install bla
sudo dpkg -i <file>.deb
# to remove all related packages and config files
sudo apt-get purge bla
```

## Convert .bib to .yaml

```bash
pandoc-citeproc -y library.bib > library.yaml
```


## Tinytex for R

```bash
# search the package containing an specific style
tlmgr search --global --file "/trimspaces.sty"
# install a package
tlmgr install trimspaces
```

## Check for badclocks

```bash
sudo fdisk -l # check block size
badblocks -sv /dev/nvme0n1 # only-reading
badblocks -nsv /dev/nvme0n1 # writing-reading (non-destructive)
badblocks -wsv /dev/nvme0n1 # writing-reading (destructive)
badblocks -wsv -o badblocks.txt /dev/nvme0n1 # writing-reading (destructive) and save
```

11169-11223 414725-414727

PCIe SSD

e2fsck -c
mke2fs


## Sambe

```bash
- "smb://lancs/Silo/FST/LEC/GIS/GISresearch/Projects/FoodInsecurity"
sudo apt-get install cifs-utils
sudo apt-get install smbclient


smb:\\lancs\Silo\FST\LEC\GIS\GISresearch\Projects\FoodInsecurity

smb://homes.lancs.ac.uk/53/chaconmo
```

## Symbolic link

```bash

# soft link
touch link_test.R
ln -s link_test.R my-link
# remove soft link
rm my-link
```

## cores info

```bash
cat /proc/cpuinfo
```

## splits pdf

```bash
pdfseparate -f 1 -l 1 doc.pdf bachiller.pdf
pdfunite mtc5.pdf mtc6.pdf mtc.pdf
~/
```

## Ag or grep

```bash

ag bla
ag -G "pattern"
ag --r function
ag -G '\.R$' 'ftp' .
```

## Network settings

```bash
nm-connection-editor
```

```bash
# check networks and drivers
 sudo lshw -C network
# check  if wifi is blocked
rfkill list all; ifconfig
# check kernel version
uname -r
```



