# My Nvim configuration

Here is a brief description of my Nvim configuration to remind me why I decided
that setup, and fix some problems I found out.

## Notes taking

Plugins:
- bullets.vim
- pandocs.vim

## Markdown, Rmarkdown, pandoc, notes

Text automatic wraping is nice but have problems with other plugins, I prefer
to wrap as we write and update paragraph if desired.

## Snippets

Most common formats are `ultisnips` and `snipmate`. Common plugins are
`ultisnips` and `neosnippets.vim`. Although Ultisnips have more features, it
seems to have problems in Nvim. The package `neosnippets.vim` was done
explicitly for Nvim and works faster than `ultisnips`. Hence, I am using
`neosnippets.vim` now.

- [ ] How to create snippets using neosnippets.

## Bugs

- `--INSERT--` blinking when writing long paragraphs in markdown and deoplete
  option. Seems to be deoplete or Nvim problem.

verbose set textwidth?


setlocal concealcursor-=nvic formatoptions=tnw showbreak=>\
troqn



