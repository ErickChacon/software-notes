##

# Docker concepts

- Images: Isolated operating systems, cooking recipe for installation.
- Containers: Instances of images, not persistent.
- Volumes: Underlying data that can be used across containers.

# Docker installation

From [askubuntu](https://askubuntu.com/questions/938700/how-do-i-install-docker-on-ubuntu-16-04-lts?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
and
[digitalocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04)

```bash
# Setup docker repository
sudo apt-get update
sudo apt-get install apt-transport-https -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# Install docker community edition
sudo apt-get update
sudo apt-get install docker-ce -y
# Verifying installation
docker run hello-world
# Add the current user to the docker group
sudo usermod -a -G docker $USER
```

# Docker use

```bash
docker --version
docker info
```

```bash
# list of containers
docker container ls -all
docker ps -a
docker run --name my-container hello-world
docker ps -a
# list of images
docker images
docker image ls
```
  unable to start device X11cairo
  )
```bash
# build a docker image from a dockerfile
docker build -t my-ubuntu-image .
docker build --build-arg user1=toolbox -t my-r .
docker build -t my-r .
docker build -t erickchacon/stat-toolbox:3.6.0 .
```

```bash
# run a docker image
docker run -it ubuntu bash
docker run -it --name my-linux --rm -v ~/Documents/Repositories:/my-data ubuntu bash
docker run --rm -p 8787:8787 -e PASSWORD=aaaaaaaa my-r #rstudio
docker run --rm -p 8787:8787 -e USER=plop -e PASSWORD=plop rocker/geospatial
docker run --rm -it --user rstudio rocker/geospatial bash
docker run --rm --user chaconmo -v $(pwd):/home/chaconmo/ -it my-r bash
docker run --rm --user chaconmo -v /home/chaconmo/Documents/:/home/chaconmo/Documents -it my-r bash

XSOCK=/tmp/.X11-unix && \
  XAUTH=/tmp/.docker.xauth && \
  xauth nlist :0 | sed -e "s/^..../ffff/" | xauth -f $XAUTH nmerge - && \
  docker run --user chaconmo -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH \
  -v /home/chaconmo/Documents/:/home/chaconmo/Documents \
  -e XAUTHORITY=$XAUTH -e DISPLAY=$DISPLAY --rm -it my-r bash

XSOCK=/tmp/.X11-unix && \
  XAUTH=/tmp/.docker.xauth && \
  xauth nlist :0 | sed -e "s/^..../ffff/" | xauth -f $XAUTH nmerge - && \
  docker run --user rstudio -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH \
  -v /home/chaconmo/Documents/:/home/rstudio/Documents \
  -e XAUTHORITY=$XAUTH  -e DISPLAY=$DISPLAY --rm -it my-r bash

docker run --rm -p 8787:8787 -e PASSWORD=erick \
  -v /home/chaconmo/Documents/:/home/rstudio/Documents my-r

docker run --user rstudio --name global-docker \
  -v /home/chaconmo/Documents/:/home/rstudio/Documents \
  --rm -it -e "TERM=xterm-256color-italic" my-r bash

docker run --user rstudio --name global-docker \
  -v /home/chaconmo/Documents/:/home/rstudio/Documents \
  -v /home/chaconmo/.bash_it/aliases/custom.aliases.bash:/home/rstudio/custom.alias.bash \
  --rm -it -e "TERM=xterm-256color-italic" my-r bash


```

```bash
docker system prune # remove dangling resources
docker system prune -a # remove stopped containers and unused images
```

# Docker configuration for linux

```bash
# add user with its home directory with default bash
RUN useradd -ms /bin/bash newuser
USER newuser
WORKDIR /home/newuser
```

```bash
XSOCK=/tmp/.X11-unix && XAUTH=/tmp/.docker.xauth && xauth nlist :0 | sed -e \
"s/^..../ffff/" | xauth -f $XAUTH nmerge - && docker run  -v $XSOCK:$XSOCK -v \
$XAUTH:$XAUTH -e XAUTHORITY=$XAUTH  -e DISPLAY=$DISPLAY --rm -it my-ubuntu-image

XSOCK=/tmp/.X11-unix && XAUTH=/tmp/.docker.xauth && xauth nlist :0 | sed -e \
"s/^..../ffff/" | xauth -f $XAUTH nmerge - && docker run  -v $XSOCK:$XSOCK -v \
$XAUTH:$XAUTH -e XAUTHORITY=$XAUTH  -e DISPLAY=$DISPLAY --rm -it rocker/geospatial bash

XSOCK=/tmp/.X11-unix && XAUTH=/tmp/.docker.xauth && xauth nlist :0 | sed -e \
"s/^..../ffff/" | xauth -f $XAUTH nmerge - && docker run  -v $XSOCK:$XSOCK -v \
$XAUTH:$XAUTH -e XAUTHORITY=$XAUTH  -e DISPLAY=$DISPLAY --rm -it my-r bash

docker run --rm -p 8787:8787 -e PASSWORD=contrasena erickchacon/stat-toolbox
localhost:8787

```

# Docker push

```bash
docker login --username=erickchacon
docker images
docker tag ${image_id} erickchacon/stat-toolbox:latest
docker push erickchacon/stat-toolbox
```

# Docker save

```bash
docker save erickchacon/stat-toolbox > stat-toolbox.tar
docker load --input stat-toolbox.tar
```
## docker compose

`tty` can be used to start container with bash compatibility. 
[docker bash](https://stackoverflow.com/questions/51228904/how-to-run-it-with-docker-compose)

`stdin_open: true` corresponds to `-i`
`tty: true` corresponds to `-t`
